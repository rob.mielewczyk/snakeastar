# Snake
Snake with self solving Algorithms

### Prerequisites
```
Python3
pygame
```

### How to run
```
git clone <this-url>
pip install pygame
python Game.py
```
#### A* algorithm example
![Snake - A*](./readme_files/snake.gif)
